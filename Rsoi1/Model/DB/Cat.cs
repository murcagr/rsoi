﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rsoi_1.Model
{
    public class Cat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Breed { get; set; }

        public Cat(string Name, string Breed) {
            this.Name = Name;
            this.Breed = Breed;
        }
    }
}
