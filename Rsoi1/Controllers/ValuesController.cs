﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rsoi_1.Model;

namespace Rsoi_1.Controllers
{
    [Route("api/cats")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private AppDBContext _context;

        public ValuesController(AppDBContext context)
        {
            _context = context;
        }

        // GET api/cats
        [HttpGet]
        public ActionResult<IEnumerable<Cat>> Get()
        {
            List<Cat> cats = _context.Cats.ToList();
            
            return cats;
        }

        // GET api/cats/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/cats
        [HttpPost]
        public IActionResult Post([FromBody] Cat cat)
        {
            try
            {
                _context.Cats.Add(cat);
                _context.SaveChanges();
                return Ok($"Cat {cat.Name} with {cat.Breed} breed was added");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error");
            }

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
